import { config } from "../config/config";
import { assert, Assertion } from 'chai';
import { browser } from 'protractor';
import { Given, When, Then } from 'cucumber';
import { title } from 'process';
import { PageLogin } from '../pages/UserInyerface/PageLogin';


var pageLogin = new PageLogin();

Given('que ingreso a la aplicacion de userinyerface', async function (){
        
    await browser.manage().window().maximize();
    console.log("Url del Ambiente de Ejecucion =>", browser.params.urlPortal);
    browser.get(browser.params.urlPortal);

});

Then('verifico las imagenes y textos de la pantalla inicial', async function () {

    await pageLogin.displayedWebElement(pageLogin.ImgUserInyerface);
    await pageLogin.HighLight(pageLogin.TextOne);
    assert.isString(await pageLogin.getInnerText(pageLogin.TextOne));
    await pageLogin.HighLight(pageLogin.TextTwo);
    assert.isString(await pageLogin.getInnerText(pageLogin.TextTwo));
    await pageLogin.HighLight(pageLogin.BtnNo);
    
});


When('hago click HERE', async function () {

    await pageLogin.HighLight(pageLogin.BtnHere);
    await pageLogin.clickOnWebElement(pageLogin.BtnHere);
    browser.sleep(5000);
  
});


